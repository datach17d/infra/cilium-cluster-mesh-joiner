all: build test vet lint

build:
	go build -ldflags "-X main.version=$(shell git describe --tags)" -o cilium-joiner ./

test:
	go test ./...

coverage.out:
	go test ./... -coverprofile=coverage.out -covermode count

coverage.xml: coverage.out
	gocover-cobertura < $< > $@

coverage.html: coverage.out
	go tool cover -html=$< -o $@

test_coverage: coverage.out coverage.xml report.xml coverage.html
	go tool cover -func=$<

report.xml:
	gotestsum --junitfile report.xml --format testname

vet:
	go vet ./

lint:
	golangci-lint run --timeout 5m --issues-exit-code 0 --out-format code-climate ./... > gl-code-quality-report.json

clean:
	$(RM) ./coverage.*
	$(RM) ./report.xml
	$(RM) ./cilium-joiner
	$(RM) ./gl-code-quality-report.json
