package main

import (
	"log"
	"os"
	"strconv"
	"time"

	ciliumjoiner "gitlab.com/datach17d/infra/cilium-cluster-mesh-joiner/pkg"
)

var version string = "dev"
var pollinterval int = 0

const pollvar string = "CILIUM_JOINER_POLL_INTERVAL"

func main() {

	var err error
	pollinterval_str := os.Getenv(pollvar)

	if pollinterval_str != "" {
		pollinterval, err = strconv.Atoi(pollinterval_str)
		if err != nil {
			log.Fatalf("%s value is not integer (got '%s')", pollvar, pollinterval_str)
		}
	}

	for {
		log.Printf("Cilium-joiner version: %s", version)
		err = ciliumjoiner.GetCiliumClusterMeshConfig()

		if err != nil {
			log.Printf("Error: %#v", err)
		}

		if pollinterval > 0 {
			log.Printf("Running in %d seconds...", pollinterval)
			time.Sleep(time.Duration(pollinterval) * time.Second)
		}

	}

}
