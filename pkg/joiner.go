package ciliumjoiner

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	corev1a "k8s.io/client-go/applyconfigurations/core/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

type Constants struct {
	CiliumNameSpace                string
	ConfigMapName                  string
	ConfigMapClusterName           string
	ConfigMapClusterID             string
	ClusterMeshServiceName         string
	ClusterMeshAPIServerCASecret   string
	ClusterMeshAPIServerRemoteCert string
	CACert                         string
	TLSCert                        string
	TLSCertKey                     string
	CiliumConfigSecretName         string
}

var Default = &Constants{
	CiliumNameSpace:                "kube-system",
	ConfigMapName:                  "cilium-config",
	ConfigMapClusterName:           "cluster-name",
	ConfigMapClusterID:             "cluster-id",
	ClusterMeshServiceName:         "clustermesh-apiserver",
	ClusterMeshAPIServerCASecret:   "clustermesh-apiserver-server-cert",
	ClusterMeshAPIServerRemoteCert: "clustermesh-apiserver-remote-cert",
	CACert:                         "ca.crt",
	TLSCert:                        "tls.crt",
	TLSCertKey:                     "tls.key",
	CiliumConfigSecretName:         "cilium-clustermesh",
}

type clusterInformation struct {
	ClusterName string
	ClusterID   string
	ServiceURL  string
	ServiceCA   []byte
	ClientCert  []byte
	ClientKey   []byte
}

type CiliumClusterMeshConfig struct {
	K8sClientSet      kubernetes.Interface
	ClusterInfo       *clusterInformation
	defaults          *Constants
	RemoteClusterInfo []*CiliumClusterMeshConfig
}

func NewConfig(clientset kubernetes.Interface, defaults *Constants) (*CiliumClusterMeshConfig, error) {

	ci, err := extractInformation(clientset, defaults)

	if err != nil {
		log.Printf("Error getting cilium information: %v\n", err)
		return nil, err
	}

	log.Printf("Got cilium information...\n   Cluster Name: %s\n   ClusterMesh URL: %s\n", ci.ClusterName, ci.ServiceURL)

	return &CiliumClusterMeshConfig{
		K8sClientSet: clientset,
		ClusterInfo:  ci,
		defaults:     defaults,
	}, nil
}

func (ciliumConfig *CiliumClusterMeshConfig) GetRemoteClusterInfo() error {

	configs, err := getKubeConfigSecrets(ciliumConfig.K8sClientSet)

	if err != nil {
		return err
	}

	clientsets, err := getClientSets(configs)

	if err != nil {
		return err
	}

	return ciliumConfig.fetchRemoteClusterInfo(clientsets)
}

func (ciliumConfig *CiliumClusterMeshConfig) fetchRemoteClusterInfo(clientsets []kubernetes.Interface) error {
	for _, c := range clientsets {
		cc, err := NewConfig(c, ciliumConfig.defaults)

		if err != nil {
			return err
		}

		cc.RemoteClusterInfo = append(cc.RemoteClusterInfo, ciliumConfig)
		ciliumConfig.RemoteClusterInfo = append(ciliumConfig.RemoteClusterInfo, cc)
	}

	return nil
}

func (ciliumConfig *CiliumClusterMeshConfig) applyCiliumClusterMeshConfig() error {

	configList := make([]*clusterInformation, len(ciliumConfig.RemoteClusterInfo))

	for i, rci := range ciliumConfig.RemoteClusterInfo {
		configList[i] = rci.ClusterInfo
	}

	secretConfig := generateSecretData(configList)

	_, err := ciliumConfig.K8sClientSet.CoreV1().Secrets(ciliumConfig.defaults.CiliumNameSpace).Get(context.Background(), ciliumConfig.defaults.CiliumConfigSecretName, metav1.GetOptions{})

	if err != nil {
		log.Printf("Creating %s/%s for cluster %s", ciliumConfig.defaults.CiliumNameSpace, ciliumConfig.defaults.CiliumConfigSecretName, ciliumConfig.ClusterInfo.ClusterName)
		configSecret := &corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      ciliumConfig.defaults.CiliumConfigSecretName,
				Namespace: ciliumConfig.defaults.CiliumNameSpace,
			},
			Data: secretConfig,
			Type: corev1.SecretTypeOpaque,
		}
		_, err = ciliumConfig.K8sClientSet.CoreV1().Secrets(ciliumConfig.defaults.CiliumNameSpace).Create(context.Background(), configSecret, metav1.CreateOptions{})
	} else {
		log.Printf("Updating %s/%s for cluster %s", ciliumConfig.defaults.CiliumNameSpace, ciliumConfig.defaults.CiliumConfigSecretName, ciliumConfig.ClusterInfo.ClusterName)

		//TODO: do diff on secrete fields and only apply on change
		configSecret := &corev1a.SecretApplyConfiguration{
			Data: secretConfig,
		}
		var data []byte
		data, err = json.Marshal(configSecret)

		if err != nil {
			return err
		}

		_, err = ciliumConfig.K8sClientSet.CoreV1().Secrets(ciliumConfig.defaults.CiliumNameSpace).Patch(context.Background(), ciliumConfig.defaults.CiliumConfigSecretName, types.StrategicMergePatchType, data, metav1.PatchOptions{})
	}

	return err
}

func (ciliumConfig *CiliumClusterMeshConfig) ApplyAllConfig() error {
	for _, ccRemote := range ciliumConfig.RemoteClusterInfo {
		err := ccRemote.applyCiliumClusterMeshConfig()

		if err != nil {
			return err
		}
	}

	return ciliumConfig.applyCiliumClusterMeshConfig()
}

func extractInformation(clientset kubernetes.Interface, defaults *Constants) (*clusterInformation, error) {
	ci := &clusterInformation{}
	var ok bool

	cm, err := clientset.CoreV1().ConfigMaps(defaults.CiliumNameSpace).Get(context.Background(), defaults.ConfigMapName, metav1.GetOptions{})

	if err != nil {
		return nil, fmt.Errorf("Unable to retrieve ConfigMap %q: %v", defaults.ConfigMapName, err)
	}

	if ci.ClusterName, ok = cm.Data[defaults.ConfigMapClusterName]; !ok {
		return nil, fmt.Errorf("Configmap %q does not contain key %q", defaults.ConfigMapName, defaults.ConfigMapClusterName)
	}

	if ci.ClusterID, ok = cm.Data[defaults.ConfigMapClusterID]; !ok {
		return nil, fmt.Errorf("Configmap %q does not contain key %q", defaults.ConfigMapName, defaults.ConfigMapClusterID)
	}

	svc, err := clientset.CoreV1().Services(defaults.CiliumNameSpace).Get(context.Background(), defaults.ClusterMeshServiceName, metav1.GetOptions{})
	if err != nil {
		return nil, fmt.Errorf("Unable to retrieve ConfigMap %q: %v", defaults.ClusterMeshServiceName, err)
	}

	switch {
	case svc.Spec.Type == corev1.ServiceTypeLoadBalancer:
		if len(svc.Spec.Ports) > 0 {
			service_ip := ""

			if len(svc.Status.LoadBalancer.Ingress) > 0 {
				service_ip = svc.Status.LoadBalancer.Ingress[0].IP
			} else {
				service_ip = svc.Spec.LoadBalancerIP
			}

			if service_ip != "" {
				ci.ServiceURL = fmt.Sprintf("https://%s:%d", service_ip, svc.Spec.Ports[0].Port)
			}
		}
	default:
		return nil, fmt.Errorf("Unsupported service type: %v", svc.Spec.Type)
	}

	caCRT, err := clientset.CoreV1().Secrets(defaults.CiliumNameSpace).Get(context.Background(), defaults.ClusterMeshAPIServerCASecret, metav1.GetOptions{})

	if err != nil {
		return nil, fmt.Errorf("Unable to retrieve Secret %q: %v", defaults.ClusterMeshAPIServerCASecret, err)
	}

	if ci.ServiceCA, ok = caCRT.Data[defaults.CACert]; !ok {
		return nil, fmt.Errorf("Secret %q does not contain key %q", defaults.ClusterMeshAPIServerCASecret, defaults.CACert)
	}

	remoteCRT, err := clientset.CoreV1().Secrets(defaults.CiliumNameSpace).Get(context.Background(), defaults.ClusterMeshAPIServerRemoteCert, metav1.GetOptions{})

	if err != nil {
		return nil, fmt.Errorf("Unable to retrieve Secret %q: %v", defaults.ClusterMeshAPIServerRemoteCert, err)
	}

	if ci.ClientCert, ok = remoteCRT.Data[defaults.TLSCert]; !ok {
		return nil, fmt.Errorf("Secret %q does not contain key %q", defaults.ClusterMeshAPIServerRemoteCert, defaults.TLSCert)
	}

	if ci.ClientKey, ok = remoteCRT.Data[defaults.TLSCertKey]; !ok {
		return nil, fmt.Errorf("Secret %q does not contain key %q", defaults.ClusterMeshAPIServerRemoteCert, defaults.TLSCertKey)
	}

	return ci, nil
}

func generateSecretData(ciList []*clusterInformation) map[string][]byte {
	data := make(map[string][]byte)

	//TODO: check for duplicate cluster names/IDs

	for _, ci := range ciList {
		config := fmt.Sprintf("endpoints:\n- %s\n", ci.ServiceURL) +
			fmt.Sprintf("trusted-ca-file: '/var/lib/cilium/clustermesh/%s-ca.crt'\n", ci.ClusterName) +
			fmt.Sprintf("cert-file: '/var/lib/cilium/clustermesh/%s.crt'\n", ci.ClusterName) +
			fmt.Sprintf("key-file: '/var/lib/cilium/clustermesh/%s.key'\n", ci.ClusterName)

		data[ci.ClusterName] = []byte(config)
		data[fmt.Sprintf("%s-ca.crt", ci.ClusterName)] = ci.ServiceCA
		data[fmt.Sprintf("%s.crt", ci.ClusterName)] = ci.ClientCert
		data[fmt.Sprintf("%s.key", ci.ClusterName)] = ci.ClientKey
	}

	return data

}

func generateConfigs(secretName string, secretNamespace string, ciList []*clusterInformation) *corev1a.SecretApplyConfiguration {
	configSecret := corev1a.Secret(secretName, secretNamespace)
	configSecret.Data = generateSecretData(ciList)

	return configSecret
}

func getKubeConfigSecrets(clientset kubernetes.Interface) ([]corev1.Secret, error) {
	var kubeConfigs []corev1.Secret

	secretList, err := clientset.CoreV1().Secrets("").List(context.Background(), metav1.ListOptions{
		LabelSelector: "cluster.x-k8s.io/cluster-name",
	})

	if err != nil {
		return nil, err
	}

	for _, secret := range secretList.Items {
		if strings.Contains(secret.ObjectMeta.Name, "-kubeconfig") {
			if _, ok := secret.Data["value"]; ok {
				kubeConfigs = append(kubeConfigs, secret)
			}
		}
	}

	return kubeConfigs, nil
}

func getClientSets(secrets []corev1.Secret) ([]kubernetes.Interface, error) {

	var clientsets []kubernetes.Interface

	for _, kubeConfigSecret := range secrets {
		kubeConfig, err := clientcmd.RESTConfigFromKubeConfig(kubeConfigSecret.Data["value"])

		if err != nil {
			return nil, err
		}

		clientset, err := kubernetes.NewForConfig(kubeConfig)

		if err != nil {
			return nil, err
		}

		clientsets = append(clientsets, clientset)
	}

	return clientsets, nil
}

func GetCiliumClusterMeshConfig() (err error) {

	default_path := clientcmd.NewDefaultClientConfigLoadingRules().GetDefaultFilename()

	log.Printf("Trying kubeconfig: %s", default_path)
	kubeConfig, err := clientcmd.BuildConfigFromFlags("", default_path)

	if err != nil {
		log.Printf("Error: %v\n", err)

		kubeConfig, err = rest.InClusterConfig()

		if err != nil {
			fmt.Printf("Error: %v\n", err)
			return
		}

		log.Printf("Using in cluster config...")
	}

	clientset, err := kubernetes.NewForConfig(kubeConfig)

	if err != nil {
		log.Printf("Error: %v\n", err)
		return
	}

	mainConfig, err := NewConfig(clientset, Default)

	if err != nil {
		log.Printf("Error: %v\n", err)
		return
	}

	err = mainConfig.GetRemoteClusterInfo()

	if err != nil {
		log.Printf("Error: %v\n", err)
		return
	}

	err = mainConfig.ApplyAllConfig()

	if err != nil {
		log.Printf("Error: %v\n", err)
		return
	}

	return
}
