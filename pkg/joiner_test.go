package ciliumjoiner

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/fake"

	"k8s.io/client-go/tools/clientcmd"
	clientcmdapi "k8s.io/client-go/tools/clientcmd/api"
)

func generateCluster(clusterName string, clusterID string, serviceIP string, servicePort int32, serviceIPstatus bool) []runtime.Object {

	service_status := corev1.ServiceStatus{}

	if serviceIPstatus {
		service_status.LoadBalancer = corev1.LoadBalancerStatus{
			Ingress: []corev1.LoadBalancerIngress{
				{IP: serviceIP},
			},
		}
	}

	return []runtime.Object{
		&corev1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "cilium-config",
				Namespace: "kube-system",
			},
			Data: map[string]string{
				"cluster-id":   clusterID,
				"cluster-name": clusterName,
			},
		},
		&corev1.Service{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "clustermesh-apiserver",
				Namespace: "kube-system",
			},
			Spec: corev1.ServiceSpec{
				Type:           corev1.ServiceTypeLoadBalancer,
				LoadBalancerIP: serviceIP,
				Ports: []corev1.ServicePort{
					{
						Port: servicePort,
					},
				},
			},
			Status: service_status,
		},
		&corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "clustermesh-apiserver-server-cert",
				Namespace: "kube-system",
			},
			Data: map[string][]byte{
				"ca.crt":  []byte("ca"),
				"tls.crt": []byte("ca-tls-crt"),
				"tls.key": []byte("ca-tls-key"),
			},
		},
		&corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "clustermesh-apiserver-remote-cert",
				Namespace: "kube-system",
			},
			Data: map[string][]byte{
				"ca.crt":  []byte("remote-ca"),
				"tls.crt": []byte("remote-tls-crt"),
				"tls.key": []byte("remote-tls-key"),
			},
		},
	}
}

func TestExtractInfo(t *testing.T) {
	assert := assert.New(t)

	clusterName := "test"
	clusterID := "1"
	serviceIP := "1.1.1.1"
	servicePort := int32(2379)

	objects := generateCluster(clusterName, clusterID, serviceIP, servicePort, true)

	k8sClient := fake.NewSimpleClientset(objects...)

	ci, err := extractInformation(k8sClient, Default)

	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}

	if ci == nil {
		t.Fatal("Cluster information is nil")
	}

	assert.Equal(ci.ClusterName, clusterName, "ClusterName is not correct")
	assert.Equal(ci.ServiceURL, fmt.Sprintf("https://%s:%d", serviceIP, servicePort), "ServiceURL is not correct")
	assert.Equal(ci.ServiceCA, []byte("ca"), "ServiceCA is not correct")
	assert.Equal(ci.ClientCert, []byte("remote-tls-crt"), "ClientCert is not correct")
	assert.Equal(ci.ClientKey, []byte("remote-tls-key"), "ClientKey is not correct")
}

func TestExtractInfoMissingServiceStatus(t *testing.T) {
	assert := assert.New(t)

	clusterName := "test"
	clusterID := "1"
	serviceIP := "1.1.1.1"
	servicePort := int32(2379)

	objects := generateCluster(clusterName, clusterID, serviceIP, servicePort, false)

	k8sClient := fake.NewSimpleClientset(objects...)

	ci, err := extractInformation(k8sClient, Default)

	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}

	if ci == nil {
		t.Fatal("Cluster information is nil")
	}

	assert.Equal(clusterName, ci.ClusterName, "ClusterName is not correct")
	assert.Equal(fmt.Sprintf("https://%s:%d", serviceIP, servicePort), ci.ServiceURL, "ServiceURL is not correct")
	assert.Equal([]byte("ca"), ci.ServiceCA, "ServiceCA is not correct")
	assert.Equal([]byte("remote-tls-crt"), ci.ClientCert, "ClientCert is not correct")
	assert.Equal([]byte("remote-tls-key"), ci.ClientKey, "ClientKey is not correct")
}

func TestGenerateConfig(t *testing.T) {
	assert := assert.New(t)
	ci := []*clusterInformation{
		{
			ClusterName: "cluster1",
			ClusterID:   "1",
			ServiceURL:  "https://1.1.1.1:1234",
			ServiceCA:   []byte("ca-tls-crt"),
			ClientCert:  []byte("ca-tls-key"),
			ClientKey:   []byte("ca-tls-key"),
		},
		{
			ClusterName: "cluster2",
			ClusterID:   "1",
			ServiceURL:  "https://2.2.2.2:1234",
			ServiceCA:   []byte("ca-tls-crt"),
			ClientCert:  []byte("ca-tls-key"),
			ClientKey:   []byte("ca-tls-key"),
		},
	}

	cluster1_expectedConfigString := `endpoints:
- https://1.1.1.1:1234
trusted-ca-file: '/var/lib/cilium/clustermesh/cluster1-ca.crt'
cert-file: '/var/lib/cilium/clustermesh/cluster1.crt'
key-file: '/var/lib/cilium/clustermesh/cluster1.key'
`

	cluster2_expectedConfigString := `endpoints:
- https://2.2.2.2:1234
trusted-ca-file: '/var/lib/cilium/clustermesh/cluster2-ca.crt'
cert-file: '/var/lib/cilium/clustermesh/cluster2.crt'
key-file: '/var/lib/cilium/clustermesh/cluster2.key'
`

	//expectedSecret := corev1a.Secret(Default.CiliumConfigSecretName, Default.CiliumNameSpace)
	expectedSecret := map[string][]byte{
		"cluster1":        []byte(cluster1_expectedConfigString),
		"cluster1-ca.crt": []byte("ca-tls-crt"),
		"cluster1.crt":    []byte("ca-tls-key"),
		"cluster1.key":    []byte("ca-tls-key"),
		"cluster2":        []byte(cluster2_expectedConfigString),
		"cluster2-ca.crt": []byte("ca-tls-crt"),
		"cluster2.crt":    []byte("ca-tls-key"),
		"cluster2.key":    []byte("ca-tls-key"),
	}

	generateConfigs := generateSecretData(ci)

	assert.Equal(expectedSecret["cluster1"], generateConfigs["cluster1"], "Cluster 1 config incorrect")
	assert.Equal(expectedSecret["cluster2"], generateConfigs["cluster2"], "Cluster 2 config incorrect")
	assert.Equal(expectedSecret, generateConfigs, "Generated config is incorret")
}

func TestGetKubeConfigSecrets(t *testing.T) {
	objects := []runtime.Object{
		&corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "cluster1-kubeconfig",
				Namespace: "cluster1",
				Labels: map[string]string{
					"cluster.x-k8s.io/cluster-name": "cluster1",
				},
			},
			Data: map[string][]byte{
				"value": []byte("kubeconfig"),
			},
		},
		&corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "cluster1-ca",
				Namespace: "cluster1",
				Labels: map[string]string{
					"cluster.x-k8s.io/cluster-name": "cluster1",
				},
			},
			Data: map[string][]byte{
				"tls.crt": []byte("crt"),
				"tls.key": []byte("key"),
			},
		},
		&corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "cluster2-kubeconfig",
				Namespace: "cluster2",
				Labels: map[string]string{
					"cluster.x-k8s.io/cluster-name": "cluster2",
				},
			},
			Data: map[string][]byte{
				"value": []byte("kubeconfig"),
			},
		},
		&corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "cluster2-ca",
				Namespace: "cluster2",
				Labels: map[string]string{
					"cluster.x-k8s.io/cluster-name": "cluster2",
				},
			},
			Data: map[string][]byte{
				"tls.crt": []byte("crt"),
				"tls.key": []byte("key"),
			},
		},
		&corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "clustermesh-apiserver-server-cert",
				Namespace: "kube-system",
			},
			Data: map[string][]byte{
				"ca.crt":  []byte("ca"),
				"tls.crt": []byte("ca-tls-crt"),
				"tls.key": []byte("ca-tls-key"),
			},
		},
	}

	k8sClient := fake.NewSimpleClientset(objects...)

	configs, err := getKubeConfigSecrets(k8sClient)

	if err != nil {
		t.Fatalf("Unexpect error: %v", err)
	}

	if len(configs) != 2 {
		t.Fatalf("Incorrect number of config secrets %d", len(configs))
	}

}

func TestGetClientSetsFromSecret(t *testing.T) {

	kubeconfig := clientcmdapi.NewConfig()

	kubeconfig.APIVersion = "v1"
	kubeconfig.Clusters = map[string]*clientcmdapi.Cluster{
		"default": {
			Server: "https://1.1.1.1:6443",
		},
	}
	kubeconfig.AuthInfos = map[string]*clientcmdapi.AuthInfo{
		"default": {},
	}
	kubeconfig.CurrentContext = "default"
	kubeconfig.Kind = "Config"
	kubeconfig.Contexts = map[string]*clientcmdapi.Context{
		"default": {
			Cluster:  "default",
			AuthInfo: "default",
		},
	}

	kubeconfigRaw, _ := clientcmd.Write(*kubeconfig)

	configs := []corev1.Secret{
		{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "cluster1-kubeconfig",
				Namespace: "cluster1",
				Labels: map[string]string{
					"cluster.x-k8s.io/cluster-name": "cluster1",
				},
			},
			Data: map[string][]byte{
				"value": []byte(kubeconfigRaw),
			},
		},
	}

	clientsets, err := getClientSets(configs)

	if err != nil {
		t.Fatalf("Unexpect error: %v", err)
	}

	if len(clientsets) != 1 {
		t.Fatalf("Incorrect number of config secrets %d", len(configs))
	}
}

func TestCiliumClusterMeshConfig(t *testing.T) {
	assert := assert.New(t)
	cluster1_clusterName := "cluster1"
	cluster1_clusterID := "1"
	cluster1_serviceIP := "1.1.1.1"
	cluster1_servicePort := int32(2379)

	cluster1_objects := generateCluster(cluster1_clusterName, cluster1_clusterID, cluster1_serviceIP, cluster1_servicePort, true)

	cluster1_k8sClient := fake.NewSimpleClientset(cluster1_objects...)

	mainConfig, err := NewConfig(cluster1_k8sClient, Default)

	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}

	assert.Equal(mainConfig.ClusterInfo.ClusterName, cluster1_clusterName, "Cluster name incorrect")

	cluster2_clusterName := "cluster2"
	cluster2_clusterID := "2"
	cluster2_serviceIP := "2.2.2.2"
	cluster2_servicePort := int32(2379)

	cluster2_objects := generateCluster(cluster2_clusterName, cluster2_clusterID, cluster2_serviceIP, cluster2_servicePort, false)

	cluster2_k8sClient := fake.NewSimpleClientset(cluster2_objects...)

	cluster3_clusterName := "cluster3"
	cluster3_clusterID := "3"
	cluster3_serviceIP := "3.3.3.3"
	cluster3_servicePort := int32(2379)

	cluster3_objects := generateCluster(cluster3_clusterName, cluster3_clusterID, cluster3_serviceIP, cluster3_servicePort, false)

	cluster3_k8sClient := fake.NewSimpleClientset(cluster3_objects...)

	mainConfig.fetchRemoteClusterInfo([]kubernetes.Interface{cluster2_k8sClient, cluster3_k8sClient})

	assert.Equal(2, len(mainConfig.RemoteClusterInfo), "Not enough remote clusters")
	assert.Equal(1, len(mainConfig.RemoteClusterInfo[0].RemoteClusterInfo), "Not enough remote clusters for remote cluster")
	assert.Equal(1, len(mainConfig.RemoteClusterInfo[1].RemoteClusterInfo), "Not enough remote clusters for remote cluster")
	assert.Equal(mainConfig, mainConfig.RemoteClusterInfo[0].RemoteClusterInfo[0], "Remote cluster's remote cluster does not match main config")

	err = mainConfig.applyCiliumClusterMeshConfig()

	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}

	cluster1_ciliumconfig, err := cluster1_k8sClient.CoreV1().Secrets(Default.CiliumNameSpace).Get(context.Background(), Default.CiliumConfigSecretName, metav1.GetOptions{})

	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}

	assert.Equal(8, len(cluster1_ciliumconfig.Data))

	err = mainConfig.ApplyAllConfig()

	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}

	cluster1_ciliumconfig, err = cluster1_k8sClient.CoreV1().Secrets(Default.CiliumNameSpace).Get(context.Background(), Default.CiliumConfigSecretName, metav1.GetOptions{})

	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}

	assert.Equal(8, len(cluster1_ciliumconfig.Data))

	cluster2_ciliumconfig, err := cluster3_k8sClient.CoreV1().Secrets(Default.CiliumNameSpace).Get(context.Background(), Default.CiliumConfigSecretName, metav1.GetOptions{})

	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}

	assert.Equal(4, len(cluster2_ciliumconfig.Data))

	cluster3_ciliumconfig, err := cluster3_k8sClient.CoreV1().Secrets(Default.CiliumNameSpace).Get(context.Background(), Default.CiliumConfigSecretName, metav1.GetOptions{})

	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}

	assert.Equal(4, len(cluster3_ciliumconfig.Data))

}
